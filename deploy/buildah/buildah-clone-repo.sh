#!/bin/sh

# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

CONTAINER=$1
SOURCE=$2
TARGET=$3

MNT=$(buildah mount "$CONTAINER")

MNT_TARGET="$MNT/$TARGET"
PARENT=$(dirname "$MNT_TARGET")

if [ ! -e "$PARENT" ] ; then
    mkdir -p "$PARENT"
fi

git clone "$SOURCE" "$MNT_TARGET"
buildah umount "$CONTAINER"

# vim: tabstop=4 expandtab shiftwidth=4
